package com.shivam.java_assignment2;

import java.sql.Statement;
import java.time.LocalDateTime;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.mysql.cj.protocol.Resultset;


//Class represents the functionality of USER
public class User_journey implements Runnable {
	
    private Statement conn;
    private Scanner scan;
	
	public User_journey(Statement stmt, Scanner scan) {
       conn=stmt;
       this.scan=scan;
	}
	
	LocalDateTime date = LocalDateTime.now();

	@Override
	//User Tread running here
	public void run() {
		
		Validation v = new Validation();
		int User_id=0;
		String user_name = "";
		String password = "";
		int x=0,y=0;
		
		//User can see the menu, buy directly or after the 1st option and can logout
		do {
			System.out.println("Choose the best option :-");
			System.out.println("1. Restaurant Menu");
			System.out.println("2. Buy Items");
			System.out.println("3. Logout");
			
			String query;
			
			int id,qty,total_cost=0;
			String item_name = "";
			
			System.out.print("Your Option : ");
		    int k=scan.nextInt();
		    
		    //Switch case for all the above options
			switch(k) {
			
			//For printing restaurant menu
			case 1:
				query = "select * from restaurant_menu";
				try {
					ResultSet rs = conn.executeQuery(query);
					System.out.println("Id   "+"    Items   "+" Cost");  
					while (rs.next()) {
			            System.out.format("%1d%15s%6d",rs.getInt(1),rs.getString(2),rs.getInt(3));
			            System.out.println();
			        }
					
				} catch (SQLException e) {
					System.out.println("Error in showing restautant menu...");
					e.printStackTrace();
				}
				break;
			
				
			// Order Items by entering Item ID
			case 2:
				System.out.println("Enter your id: ");
				User_id=scan.nextInt();
		//		if(v.valid_id(User_id)) {
				
				do{
				System.out.print("Enter Item Id : ");
				id = scan.nextInt();
				System.out.println("Enter Item Quantity :");
				qty = scan.nextInt();
				
				//This will return cost 
				query = "select Item_name,Cost from restaurant_menu where Item_id="+id;
				try {
					ResultSet rs = conn.executeQuery(query);
					while(rs.next()) {
					total_cost += rs.getInt("Cost")*qty;
					item_name += " [ Item_name: "+rs.getString(1)+"| Cost per Item: "+ rs.getInt(2)+"| Quantity: "+ qty+ " ]\n";
					}
				
				} catch (SQLException e) {
					System.out.println("Error in getting Item name and Cost of Items...");
					e.printStackTrace();
				}
				
				
				//Updating the Items bought by user in database to make order bill
				String sql = "insert into surabi.order_bill_new values ('"+date+"',"+User_id+",'"+id+"',"+qty+","+total_cost+")";

				try {
					conn.executeUpdate(sql);
				} catch (SQLException e) {
					System.out.println("Error in updating the bill info in database");
					e.printStackTrace();
				}
				
				
				System.out.println("Do you want to buy anything else?\n1. Yes\n2. No");
				x = scan.nextInt();
				}while(x!=2);
			
				
				
				System.out.println("Order Bill Description:\n"+item_name +"\n [ Total cost: "+ total_cost+" ]");
//			}
//				else
//				System.out.println("Incorrect id...");
				
				break;
				
			//logout functionality
			case 3:
				System.out.println("Logout Successfully...");
				y=1;
				break;
			
			default:
				System.out.println("Invalid Option...try again");
				break;
			}	
       	}while(y!=1);
	}

}

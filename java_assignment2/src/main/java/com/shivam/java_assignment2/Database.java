package com.shivam.java_assignment2;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

public class Database {

	
	//USER and PASS of my MySQL Database surabi
	public static Statement stmt;
	static final String USER = "root";
	static final String PASS = "1234"; 
	
	
	//Creating DB Connection and returning to other classes where we need it
	public static Statement getConnection() {
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/surabi",USER,PASS);
			stmt = conn.createStatement();
			
		}catch(Exception e) {
			System.out.println("Error in Database connection...");
			e.printStackTrace();
		}
		System.out.println("Database connected...");
		return stmt;
	}
	
	
}

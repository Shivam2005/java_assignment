package com.shivam.java_assignment2;

import java.sql.Statement;
import java.util.Scanner;

public class Main 
{
    public static void main( String[] args ) 
    {
    	
    	// taking DB connection from Database class
        Statement stmt = Database.getConnection();
        Scanner scan = new Scanner(System.in);
        int x=0;
        
        //Validation class object for validating user and admin
        Validation v = new Validation(stmt,scan);
        
        do {
        	
        	System.out.println("Select your choice:-\n1. User Login\n2. Admin Login\n3.Exit");
        	int y=scan.nextInt();
        	
        	switch(y) {
        	
        	case 1:  
        		if(v.User_validation()) {
        		//Creating User Thread
        		Thread user_journey = new Thread(new User_journey(stmt,scan));
        		user_journey.run();}
        		else
        			System.out.println("Please enter correct details...");
        		break;
        		
        	
        	case 2:
        		if(v.Admin_validation()) {
        		//Creating Admin Thread
        		Thread admin_journey = new Thread(new Admin_journey( stmt,scan));
        		admin_journey.run();}
        		else
        			System.out.println("Please enter correct details...");
        		break;
        		
        	case 3:
        		System.out.println("Successfully Exit...");
        		x=1;
        		break;
        	
        	default:
        		System.out.println("Invalid Option...try again");
        		break;
        	}
        }while(x!=1);
        
        
        //Get the only object available to use Singleton Design pattern 
        SingleObject obj = SingleObject.getInstance();

        //show the message
        obj.Message();
    }
}

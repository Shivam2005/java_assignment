package com.shivam.java_assignment2;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Validation implements Valid_Interface {

	
	private Statement stmt;
	private Scanner scan;
	
	//Constructor
	public Validation (Statement stmt, Scanner scan) {
		
		this.stmt=stmt;
		this.scan=scan;
		
	}
	
	String admin_name;
	String admin_password;
	String user_name;
	String password;
	



	public Validation() {}

	
	//This method is for validating Admin (If Admin's data exist in Admin table of surabi than its valid)
	@Override
	public boolean Admin_validation() {
		
		User_data user = new User_data();
		
		System.out.println();
		System.out.print("Enter Admin_name :");
		admin_name = scan.next();
		System.out.print("Enter Admin password: ");
		admin_password = scan.next();
		
		
		//Checking Admin's data in database and returning the same in main class
		try {
	            String query = "Select * from admin Where Admin_name='" + admin_name + "' and Admin_password='" + admin_password + "'";
	            ResultSet rs = stmt.executeQuery(query);
	            if (rs.next()) {
	                System.out.println("Admin validated...");
	                return true;
	            } else {
	                System.out.println("Invalid Admin...try again");
	                return false;
	            }

	        // You can also validate user by result size if its comes zero user is invalid else user is valid

	    } catch (SQLException err) {
	    	System.out.println("Sysyem :Error in logging...");
	        err.getStackTrace();
	    }
		return false;

		
	}

	
	//This method is for validating User (If Admin's data exist in User table of surabi than its valid)
	@Override
	public boolean User_validation() {
		
	//	User_data user = new User_data();
		
		scan.nextLine();
		System.out.print("Enter Username : ");
		user_name = scan.next();
		System.out.print("Enter Password : ");
		password = scan.next();
		
		
		try {
			
    		//Checking User's data in database and returning the same in main class
			
	         String query = "Select * from user Where User_name='" + user_name + "' and User_Password='" + password + "'";
	         ResultSet rs = stmt.executeQuery(query);
	         if (rs.next()) {
	            System.out.println("User validated...");
	            return true;
	         } else {
	            System.out.println("Invalid User...try again");
	            return false;
           }	
	    } catch (SQLException err) {
	    	System.out.println("System: Error in logging...");
	        err.getStackTrace();
	    }

		return false;
	}

//	public boolean valid_id(int id){
//		
//        String query = "Select User_id from user Where User_id='" + id + "'";
//        ResultSet rs;
//		try {
//			rs = stmt.executeQuery(query);
//			 if (rs.next()) {
//		           System.out.println("User validated...");
//		           return true;
//		        } else {
//		           System.out.println("Invalid User...try again");
//		           return false;
//		        }
//
//		} 
//		
//		catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//       		
//	}
	
}

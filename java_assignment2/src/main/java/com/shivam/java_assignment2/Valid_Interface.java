package com.shivam.java_assignment2;


//Its a interface and also we will use the factory design pattern 
public interface Valid_Interface {
	
	
	//Both the functions is used for validation purpose
	public boolean Admin_validation();
	public boolean User_validation();
	
}

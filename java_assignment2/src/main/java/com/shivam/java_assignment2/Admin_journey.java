package com.shivam.java_assignment2;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;


// This class is for Admin's functionality
public class Admin_journey implements Runnable{

	LocalDate date = LocalDate.now();
//	Validation v = new Validation();
	
	String admin_name="";
	String admin_password;
	String myans="";
	String myans2="";
	int x=0;
	
	private Statement conn;
    private Scanner scan;
	
    public Admin_journey(Statement stmt,Scanner scan) {
    	conn=stmt;
        this.scan=scan;
    }
	

	@Override
	//Admin's Thread running
	public void run() {		
		
		do {
			System.out.println("Choose your Statement : ");
			System.out.println("1. Today's all bills : ");
			System.out.println("2. Month's all bills : ");
			System.out.println("3. Logout");
			
			String query;
			
			System.out.print("Your Option: ");
			int z=scan.nextInt();
			
			switch(z) {
			//This is for printing all the bills of the day
			case 1:
				query = "Select * from order_bill_new where Ord_Date='"+date+"' ORDER BY Ord_Date DESC";
				try {
					ResultSet rs = conn.executeQuery(query);
					while(rs.next()) {
						   myans+=" [ Ord_Date: "+rs.getDate(1)+" Customer_id: "+ rs.getInt(2)+" Item_id: "+ rs.getInt(3)+" Quantity: "+rs.getInt(4)+" Total Cost: "+rs.getInt(5)+  " ]\n";
				           System.out.println();
					}
					System.out.println(myans);
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				} catch (SQLException e) {
					System.out.println("Error in getting data of the today's bill ...");
					e.printStackTrace();
				}
				break;
			
			//This is for printing all the bills of the month
			case 2:
			    query = "select * from order_bill_new where Ord_Date between '"+date.minusDays(30)+"' and '"+date+"' ORDER BY Ord_Date DESC";
			    try {
					ResultSet rs = conn.executeQuery(query);
					while(rs.next()) {
						   myans2+=" [ Ord_Date: "+rs.getDate(1)+" Customer_id: "+ rs.getInt(2)+" Item_id "+ rs.getString(3)+" Quantity: "+rs.getInt(4)+" Cost: "+rs.getInt(5)+  " ]\n";
				           System.out.println();
					}
					System.out.println(myans2);
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				} catch (SQLException e) {
					System.out.println("Error in getting monthly bill data...");
					e.printStackTrace();
				}
				break;
			
			//Admin's logout functionality
			case 3:
				System.out.println("Logout Successfully");
				x=1;
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					System.out.println("Error in logging out...");
					e.printStackTrace();
				}
				break;
				
			default:
				System.out.println("Invalid Option...try again");
				break;
			}
			
		} while(x!=1);
		
	}
}

# Maven Project named as Java_assignement2

# surabi (Database) SQL file is added in the repository.
# And below are the tables inside it:-

1. User
2. Admin
3. Restaurant menu
4. Order bill


# Note:- Please use User and Admin as entered in surabi database for validation 

e.g User: Shivam    Pass: Shivam
    Admin: Accolite  Pass: Accolite
    
    
# Note:- You might get similar ouput in Admin's Functionality i.e Todays all bill and Monthly all bills :- 
Because we are inserting the data inside the database and all the entries i updated is of today itself, so if you want to see different result, fisrt order something and then check monthly bills, you will get some extra entries too.



# All the Classes:-

1. Main : Created user and admin thread.

2. User_journey : All the user's functionality defined there with proper comments.

3. Admin_journey : All the admin's functionality defined there with proper comments.

4. Valid_Interface : Its a interface class where user's and admin's validation class is created. and it also shows the factory design pattern's use.

5. Validation : Where we are checking its a valid user/admin or not by comparing the entered data      from the surabi database.

6. Database : JDBC connection is created here.

7. SingleObject : Used for printing some message at the end and it also shows singleton pattern's used.

8. User_data : Which is used to set/get the user's name.


# Design patterns Used:-
1. Singleton DP
2. Factory DP


# Comments are properly added inside all the classes

# Try catch is used properly to handle and to print error messages


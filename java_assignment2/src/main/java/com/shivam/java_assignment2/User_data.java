package com.shivam.java_assignment2;


//This class is for getting user's information
public class User_data {
	
	
	private String user_name;
	private String password;
	private int user_id;
	
	
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public User_data() {}
	

	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}	

}

package com.shivam.java_assignment2;

public class SingleObject {

	
	   // Implemented Singleton Design pattern.
	   //created an object of SingleObject 
	   private static SingleObject instance = new SingleObject();

	   //made the constructor private so that this class cannot be
	   //instantiated
	   private SingleObject(){}

	   //Get the only object available
	   public static SingleObject getInstance(){
	      return instance;
	   }

	   
	   //Showing the below message at the end
	   public void Message(){
	      System.out.println("Have a nice day...Thank you for visiting us");
	   }
	}
/*Creating surabi restaurant database*/

create database surabi;
use surabi;


/*Creating Admin table */

create table admin(
    Admin_name char (30),
    Admin_password varchar(20)    
);

/* Valid Admins for restaurant surabi */

insert into admin values(1,'Admin','Admin');
insert into admin values(2,'Accolite','Accolite');


/* Creating User table*/

create table User(
	userid int,
    username char (30),
    userpass varchar(20)    
);

/* Valid Users table*/

insert into user values(1,'Shivam','Shivam');
insert into user values(2,'Amisha','Amisha');
insert into user values(3,'Prem','Prem');



/*Restaurant menu table*/

create table restaurant_menu(
	Item_id int primary key,
    Item_name char (40),
    Cost int
);

/* Table for all items in the restaurant menu*/

insert into fooditem values(1,'Aloo Paratha',30);
insert into fooditem values(2,'Burger',50);
insert into fooditem values(3,'Masala Chana',100);
insert into fooditem values(4,'Dosa',70);
insert into fooditem values(5,'Finger Chips',50);
insert into fooditem values(6,'Veg roll',60);
insert into fooditem values(7,'Pizza',300);
insert into fooditem values(8,'Sandwich',45);
insert into fooditem values(9,'Paneer Paratha',55);
insert into fooditem values(10,'Hot Dog',80);


/* Table for Storing the orders from user*/

create table order_bill_new(
    Ord_Date date,
    User_id int,
    Item_id int,
    Qty int,
    Total_Cost int
);